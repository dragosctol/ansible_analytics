#!/bin/bash
export PATH=/home/ansible/ansible/bin:/usr/local/bin:/usr/bin:/bin
export PYTHONPATH=/home/ansible/ansible/lib:
export ANSIBLE_LIBRARY=/home/ansible/ansible/library
export ANSIBLE_HOSTS=~/ansible/hosts

myerror() { echo " [!] $1"; exit 1; }
mywarn() { echo " [!] $1"; }
myinfo() { echo " [+] $1"; }

#EMAIL_ADRRESSES="pierre.lelievre@amoma.com yann.arthur@amoma.com"
EMAIL_ADDRESSES="dragos.chiriac@amoma.com pierre.lelievre@amoma.com trivago_eb_clicks@amoma.com"
#EMAIL_ADDRESSES="dragos.chiriac@amoma.com"
TEMPFILE=$(/bin/tempfile)
DATE=$(date  --date="yesterday" "+%d/%b/%Y")