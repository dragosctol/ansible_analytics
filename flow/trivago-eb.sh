MY_ANSIBLE_SERVERS='shopbot-servers'
MY_LOG_DIR='/var/log/apache2/availability-shop-api.algo.travel/'
MY_LOG_FILES="${MY_LOG_DIR}/access.log.1 ${MY_LOG_DIR}/access.log"
MY_LOG_FILES_GZ="${MY_LOG_DIR}/access.log.2.gz"
DATE=$(date  --date="yesterday" "+%d\/%b\/%Y")
DATE_NICE=$(date  --date="yesterday" "+%d-%b-%Y")
DATE_CSV=$(date  --date="yesterday" "+%d%m%Y")
TMPFILE='/tmp/trivago-eb.tmp'

BRANCHES=( 8654 )

analize_raw_logs_via_ansible() {
	BRANCH=$1
	OUTPUT=$(ansible  $MY_ANSIBLE_SERVERS -s -m shell -a "zcat ${MY_LOG_FILES_GZ} | cat ${MY_LOG_FILES} - | awk '\$3 ~ /trivago-eb/ && \$4 ~ /"$DATE"/ && \$7 ~ /search-bundle-availability/ && \$7 ~ /"$BRANCH"/' | wc -l" -f 30 2>&1 | egrep -v 'sbweb|No such file|^$' )
	counts=(${OUTPUT// /})
	  #echo $counts
	  let branch_total=0
	  for c in "${counts[@]}"
	  do
	    branch_total=$((branch_total+=$c))
	  done
	  ibs[$BRANCH]=$branch_total

}

create_text_report(){
	echo "Trivago Express Book for $DATE_NICE: " > $TMPFILE
	echo "" >> $TMPFILE
	echo "Branch |  Count" >> $TMPFILE
	echo "-------+-----------" >> $TMPFILE
	let total=0
	for key in "${!ibs[@]}"
	do
  		total=$((total+=${ibs[$key]}))
  		echo "${key}   |  ${ibs[$key]}" >> $TMPFILE
	done
	echo "" >> $TMPFILE
	echo "Total is $total" >> $TMPFILE
}

create_csv_report(){
	local CSV_FILE=$1
	echo "Branch,  Count" >> $CSV_FILE
	for key in "${!ibs[@]}"
	do
  		echo "${key}   ,  ${ibs[$key]}" >> $CSV_FILE
	done
}

send_report_to_email(){
	CSV_FILE="/tmp/attachement_eb_${DATE_CSV}.csv"
	create_csv_report ${CSV_FILE}
	cat $TMPFILE | EMAIL="noreply+trivago@amoma.com" mutt -s "\"Trivago EB ${DATE_NICE}\"" $EMAIL_ADDRESSES -a "${CSV_FILE}"
	rm ${CSV_FILE};rm -f $TMPFILE
}


doflow() {
	analize_raw_logs_via_ansible 8654
	create_text_report
	send_report_to_email
}
