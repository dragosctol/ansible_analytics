readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"
readonly CONF_DIR="${PROGDIR}/conf"
readonly FLOW_DIR="${PROGDIR}/flow"

source_file() {
if [ -f $1 ]; then 
    source $1; 
else 
    myerror "No $1 file"    
fi
}
source_file "${CONF_DIR}/conf.sh";

usage() {
    cat << EOF
$PROGNAME usage
EOF
}

cmdline() {
    while getopts "f:h" OPTION
    do
         case $OPTION in
         f)  readonly FLOW=$OPTARG;;
         h)  usage; exit 0 ;;
         *)  usage; exit 1 ;;
        esac
    done
    if [ -z "$FLOW" ]; then myerror "Flow must be specified"; fi
}

main() {
    cmdline $ARGS
    source_file "${FLOW_DIR}/${FLOW}.sh";
    doflow
}

main